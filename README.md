# twigExtendByPlugins

Plugin for LimeSurvey : a tool for another plugins. 
Since LimeSurvey 3.10 : usage of this plugin is deprecated , use [getPluginTwigPath](https://manual.limesurvey.org/GetPluginTwigPath) event.

## Installation

This plugin is tested with LimeSurvey 3.7.1 and can work only with 3.0.0 version and up.

### Via GIT
- Go to your LimeSurvey Directory (version up to 3.0)
- Clone [repository](https://gitlab.com/SondagesPro/twigExtendByPlugins)

### Via ZIP dowload
- Get the file zip and uncompress it
- Move the files included to plugins/twigExtendByPlugins directory

### Activate plugin

You just need to activate plugin like other plugin, see [Install and activate a plugin for LimeSurvey](https://extensions.sondages.pro/install-and-activate-a-plugin-for-limesurvey.html)

## Usage

This plugin offer a way for other plugin to replace or add new twig file. A new event  to add directory in path tested by twig.

The **getPluginTwigPath** event use 2 return value

- `TwigExtendOption` : Array of path to be tested before template, then used only if template don't have same twig file
- `TwigExtendForced` : Array of path to be tested after template, then used even if template have same twig file

A solutiuon can be to add same path to _TwigExtendForced_ and _TwigExtendOption_ but to use include a new template file in forced.

## Contribute

Issue and pull request are welcome on [gitlab](https://gitlab.com/SondagesPro/twigExtendByPlugins).


## Home page & Copyright
- Copyright © 2018 Denis Chenu <http://sondages.pro>
- Licence : GNU General Affero Public License <https://www.gnu.org/licenses/agpl-3.0.html>
