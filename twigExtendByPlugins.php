<?php
/**
 * Plugin helper for limesurvey : extend twig view by plugins
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class twigExtendByPlugins extends PluginBase {

    protected $storage = 'DbStorage';

    static protected $description = 'Plugin tool.';
    static protected $name = 'twigExtendByPlugins';

    public function init()
    {
        $lsConfig = require(APPPATH . 'config/internal' . EXT);
        $twigRenderer = $lsConfig['components']['twigRenderer'];
        Yii::setPathOfAlias('twigExtendByPlugins', dirname(__FILE__));
        Yii::import('twigExtendByPlugins.ExtendedETwigViewRenderer',true);
        $twigRenderer['class'] = 'twigExtendByPlugins.ExtendedETwigViewRenderer';
        Yii::app()->setComponent('twigRenderer',$twigRenderer);
    }

}
